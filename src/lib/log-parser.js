const fs = require('fs');
const moment = require('moment');
const os = require('os');

class LogParser {

  constructor(filePath) {
    this.filePath = filePath;
  }

  parse() {
    const file = fs.readFileSync(this.filePath, 'UTF-8');
    const rows = file.split(os.EOL);
    const results = [];

    rows.forEach((row) => {
      const columns = row.split(' ');

      if (columns.length > 5) {

        const data = {
          timestamp: moment(`${columns[0]} ${columns[1]}`),
          threadId: columns[2].slice(1, -1),
          userContext: columns[3].slice(1, -1),
          ms: parseFloat(columns[columns.length - 1]),
        };

        const isUri = columns[4].startsWith('/');
        if (isUri) {
          data.uri = columns[4];

          // Can't parse the resource name from the URI because of
          // the constraint set by the assignment: "URI + query string != resource".
          // If this constraint would have not been set then in my opinion the resource name would be "mainContent.do"
          // from an URI like: /mainContent.do?action=TOOLS&contentId=main_tools.
          // However, since the URI contains the JSP file 'mainContent.do' then one could argue that the 'resource'
          // is the action.
          const keyword = 'action=';
          const index = data.uri.indexOf(keyword);
          if (index != -1) {
            const substring = data.uri.substr(index + keyword.length);
            data.resourceName = substring.split('&')[0].toLowerCase();
          }
        } else {
          data.resourceName = columns[4];
          data.payloadElements = columns[5];
        }

        results.push(data);
      }
    });

    return results;
  }
}

module.exports = LogParser;
