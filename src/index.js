const babar = require('babar');
const hooks = require('perf_hooks');
const LogParser = require('./lib/log-parser');
const os = require('os');

class Main {
  constructor() {
    this.init();
  }

  init() {
    const t0 = hooks.performance.now();
    this.filePath = process.argv[process.argv.length - 2];
    this.numberOfRequestsToDisplay = parseInt(process.argv[process.argv.length - 1]);
    if (!this.filePath || !this.numberOfRequestsToDisplay) {
      throw Error('Please provide both relative or absolute filePath of the log file and number of resources to display');
    }
    this.analyze();
    const t1 = hooks.performance.now();
    console.log(`${os.EOL} Program run time was ${t1 - t0} ms.`);
  }

  analyze() {
    const parser = new LogParser(this.filePath);
    const results = parser.parse();

    const transformedResults = [];
    const requestsInAnHour = {};

    results.forEach((result) => {

      const hour = result.timestamp.format('HH');
      // Default number of requests in an hour is 0
      if (!requestsInAnHour[hour]) requestsInAnHour[hour] = 0;
      requestsInAnHour[hour]++;

      if (result.resourceName) {
        const transformedResult = transformedResults.find(element => element && element.name === result.resourceName);

        if (transformedResult) {
          transformedResult.ms.push(result.ms);
        } else {
          transformedResults.push({
            name: result.resourceName,
            ms: [result.ms],
          })
        }
      }
    });

    // Convert data to be suitable for drawing the histogram
    // The library needs to have continuous numbers to display data correctly (0 .. 23)
    const hours = {};
    for (let i = 0; i < 24; i++) {
      hours[i] = 0;
    }
    Object.keys(requestsInAnHour).forEach((key) => {
      const hour = parseInt(key);
      hours[hour] = requestsInAnHour[key];
    });
    const histogramData = Object.keys(hours).map((key) => {
      return [key, hours[key]];
    });

    this.drawHistogram(histogramData);

    console.log(
      `${os.EOL} Top ${this.numberOfRequestsToDisplay} resources with the highest average request duration is ms ${os.EOL}`,
    );

    this.getHighestRequestDurations(transformedResults, this.numberOfRequestsToDisplay)
      .forEach((result, index) => {
        console.log(`${index + 1}. name: ${result.name}, average duration: ${result.avg} ms`);
      });
  }

  drawHistogram(data) {
    console.log(babar(data, {
      caption: 'Hourly number of requests (Y-axis: number of requests, X-axis: hours)',
    }));
  }

  getHighestRequestDurations(data, n) {
    const averages = [];
    data.forEach(el => {

      const values = el.ms;
      const sum = values.reduce((previous, current) => current += previous);
      const avg = sum / values.length;

      averages.push({
        name: el.name,
        avg: avg,
      })
    });

    averages.sort((a, b) => b.avg - a.avg);
    return averages.slice(0, n)
  }
}

new Main();
