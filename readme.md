# Log parser and analyzer

**Test assignment.** 

Parses a log file in a specific format and displays
an **histogram** of requests per hour and **n** (n > 0; n < ∞) number of resources with the highest average request duration.

# Prerequisites
```
Node v8.9.3 or higher
```
Tested with v8.9.3 and v10.15.3. In reality v6.17.1+ should be sufficient. 
# Instructions

```bash
cd src
npm install
npm run start <pathToLogFile> <numberOfResourcesToDisplay>
```
**NB!** `<pathToLogFile>` should either be an absolute path or an relative path from **src/index.js**.



